# TP Final - Programación


#### Fecha: 10/07/2023
#### Nombre y Apellido: Walter David Wasiuk
#### Materia: Programación
#### Profesor: Sergio Pernas


##### Descripcion:

- En este script se va a poder realizar backups de ficheros a eleccion.
- Permite la istalacion y manejo del firewall de manera sencilla.
- Tambien tiene una recopilacion de comandos utiles para usar en linux (Ej: permisos y grupos).

##### USO: El mismo se interactua a traves de los comandos descriptos por el script una vez es ejecutado.

- Para el correcto funcionamiento del script es necesario abrirlo con permisos de administrador.
