#!/bin/bash

#Variables
#Entrada de informacion
Argumento=$1
#Expresion regular para que sean solo numeros
Numeros='^[0-9]+$'

#Comprobacion de root

if [ $UID -ne 0 ]; then
	echo "Por favor, ejecute este script como usuario 'root'"
	exit 1
fi

#Comprobacion de que tenga instalado UFW en el sistema

if ! which ufw > /dev/null 2>&1 ; then
	echo "Instalando el UFW"
	apt-get update > /dev/null 2>&1
	apt-get install ufw > /dev/null 2>&1 && echo "Se instalo el UFW"
fi
sleep 1s

echo "Los comandos que puede utilizar para manejar el script son los siguientes:"
echo "(Comando   = Que hace)"
echo "prender    = Prende el firewall"
echo "apagar     = Apaga el firewall"
echo "reglas     = Reglas actuales del firewall"
echo "estado     = Estado actual del firewall"
echo "limpiar    = Limpiar o resetear el firewall a DEFAULT"
echo "eliminar   = Elimina una regla especifica segun N la fila"
echo "input      = Introduce cadenas input"
echo "forward    = Introduce cadenas forward"
echo "output     = Introduce cadenas output"
echo "puerto     = Introduce reglas para puertos"
echo "añadir     = Introduce reglas a un lugar de la lista especifico"
echo "abm-ip     = Introduce reglas con origen y destino a ip y puertos"
echo "backup     = Hace un backup de las reglas"
echo ""
echo "EJEMPLO: sudo ./script-firewall.sh prender"

#Ejecucion sobre el ufw

case $Argumento in
prender)
	ufw enable
;;
apagar)
	ufw disable
;;
reglas)
	ufw show added
;;
estado)
	ufw status verbose
;;
limpiar)
	ufw reset
;;
eliminar)
        if [[ $2 == regla && $3 =~ $Numeros ]];then
                ufw delete $3 2> /dev/null || echo "Numero de regla invalido, pruebe otra"
        else
                echo "Seleccion incorrecta: Intenta con 'regla' 'numero de regla'"
        fi
;;
input)
	if [[ $2 == habilitar ]]; then
		ufw default allow incoming
	elif [[ $2 == deshabilitar ]]; then
		ufw default deny incoming
	else
		echo "Seleccion incorrecta: Intenta con 'habilitar/deshabilitar'"
	fi
;;
forward)
	if [[ $2 == habilitar ]]; then
                ufw default allow routed
	elif [[ $2 == deshabilitar ]]; then
                ufw default deny routed
	else
                echo "Seleccion incorrecta: Intenta con 'habilitar/deshabilitar'"
	fi
;;
output)
	if [[ $2 == habilitar ]]; then
		ufw default allow outgoing
	elif [[ $2 == deshabilitar ]]; then
      		ufw default deny outgoing
	else
                echo "Seleccion incorrecta: Intenta con 'habilitar/deshabilitar'"
	fi
;;
puerto)
	if [[ $2 == habilitar && $3 =~ $Numeros ]]; then
                ufw allow $3
	elif [[ $2 == deshabilitar && $3 =~ $Numeros ]]; then
                ufw deny $3
	elif [[ $2 == habilitar && $3 == 'in' && $4 =~ $Numeros ]]; then
		ufw allow in $4
	elif [[ $2 == habilitar && $3 == out && $4 =~ $Numeros ]]; then
                ufw allow out $4
	elif [[ $2 == deshabilitar && $3 == 'in' && $4 =~ $Numeros ]]; then
                ufw allow in $4
	elif [[ $2 == deshabilitar && $3 == out && $4 =~ $Numeros ]]; then
                ufw allow out $4
	else
		echo "Seleccion incorrecta: Intenta de esta manera 'habilitar/deshabilitar' 'numero del puerto' o 'habilitar/deshabilitar' 'in/out' 'numero del puerto'"
	fi
;;
añadir)
        if [[ $2 == habilitar && $3 == host && $5 == to && $7 =~ $Numero && $8 =~ $Numero ]]; then
                if [[ $4 =~ $Numeros ]]; then
                ufw insert $8 allow from $4 to $6 port $7
                fi
        elif [[ $2 == deshabilitar && $3 == host && $5 == to && $7 =~ $Numero && $8 =~ $Numero ]]; then
                if [[ $4 =~ $Numeros ]]; then
                ufw insert $8 allow from $4 to $6 port $7
                fi
        elif [[ $2 == habilitar && $3 == host && $5 == to && $7 =~ $Numero && $8 =~ $Numero ]]; then
                ufw insert $8 allow from $4 to $6 port $7
        elif [[ $2 == deshabilitar && $3 == host && $5 == to && $7 =~ $Numero && $8 =~ $Numero ]]; then
                ufw insert $8 allow from $4 to $6 port $7
        else
                echo "Seleccion incorrecta: Intenta de esta manera 'habilitar/deshabilitar' 'puerto' 'IP entrada' 'to' 'IP destino' 'numero de puerto''numero de lista'"
	fi
;;
abm-ip)
	if [[ $2 == habilitar && $3 == host && $5 == to && $7 =~ $Numeros ]]; then
		if [[ $4 =~ $Numeros ]]; then
		ufw allow from $4 to $6 port $7
		fi
        elif [[ $2 == deshabilitar && $3 == host && $5 == to && $7 =~ $Numeros ]]; then
                if [[ $4 =~ $Numeros ]]; then
		ufw deny from $4 to $6 port $7
		fi
	elif [[ $2 == habilitar && $3 == red && $5 == to && $7 =~ $Numeros ]]; then
		ufw allow from $4 to $6 port $7
	elif [[ $2 == deshabilitar && $3 == red && $5 == to && $7 =~ $Numeros ]]; then
		ufw allow from $4 to $6 port $7
	else
                echo "Seleccion incorrecta: Intenta de esta manera 'habilitar/deshabilitar' 'host/red' 'IP entrada' 'to' 'IP destino' 'numero de puerto'"
	fi
;;

#Backup de las reglas
backup)
	if [[ $2 == reglas ]]; then
		mkdir -p /var/backupfirewall
		echo "Backup de 'user.rules': $(date)" >> /var/backupfirewall/reglas.bk && \
		echo "---" >> /var/backupfirewall/reglas.bk && \
		cat /etc/ufw/user.rules >> /var/backupfirewall/reglas.bk && \
		echo "Backup de 'user6.rules': $(date)" >> /var/backupfirewall/reglas.bk && \
                echo "---" >> /var/backupfirewall/reglas.bk && \
                cat /etc/ufw/user6.rules >> /var/backupfirewall/reglas.bk && \
		echo "---" >> /var/backupfirewall/reglas.bk && \
		sleep 1s
		echo "Backup completo de 'user.rules' y 'user6.rules'."
		chmod o+r /var/backups && chmod o+r /var/backupfirewall/reglas.bk
	fi
;;
#Para los ingresos que no sean validos.
*)
	echo "Ingrese un comando valido"
	exit
;;
esac
