#!/bin/bash

# Tarea 05
# Comprobacion de root
if [ $UID -ne 0 ]; then
	echo "Por favor, ejecute este script como usuario 'root'"
	exit 1
fi

sleep 0.4s

# Variables
ARG=$1
SDATE=`date +%d-%m-%y_%H.%M.%S`
SPHP="<?php
	phpinfo();
	phpinfo(INFO_MODULES);
?>"

echo "Los comandos que puede utilizar para manejar el script son los siguientes:"
echo "(Comando    = Que hace)"
echo "modipdhcp   = Modifica la IP si actualmente tienes IP por DHCP (reinicio OS requerido)"
echo "modipstatic = Modifica la IP si actualmente tienes IP estatica (reinicio OS requerido)"
echo "modhostname = Modifica el hostname (reinicio OS requerido)"
echo "insapache   = Instala Apache"
echo "insapaphp   = Instala Apache y PHP/MySQL (Stack LAMP)"
echo "desapache   = Desinstala Apache y todas sus configs"
echo "desapaphp   = Desinstala Apache, PHP/MySQL y todas sus configs"
echo "iniciarapac = Inicia el servicio Apache"
echo "reiniciarap = Recarga Apache"
echo "crearsitio  = Crea un sitio en Apache"
echo "altasitio   = Activa sitios"
echo "bajasitio   = Desactiva sitios"
echo "remsitiovh  = Elimina el sitio, fichero de vhost"
echo "remsitiodi  = Elimina el sitio, directorio con ficheros"
echo "wordpress   = Cree sitios en wordpress"
echo "backupstvh  = Backup de sitios, fichero de vhost"
echo "backupstdi  = Backup de sitios, directorio con ficheros"
echo "migrarscpvh = Migrar fichero de vhost al servidor destino"
echo "migrarscpdi = Migrar directorio con ficheros al servidor destino"
echo "altasitiore = Activa sitios en remoto"
echo ""
echo "EJEMPLO:'sudo ./scripttarea05.sh altasitio'"
echo ""

case $ARG in
modipdhcp)
	echo "Si esta conectado remotamente probablemente pierda la conexion."
	read -p "Para poder hacer la modificacion de IP hay que cambiar a IP estatica y reiniciar para que se apliquen los cambios, continuar con el cambio <Enter> y para salir <CTRL+C>"
	read -p "Coloque su nueva IP:" PIP
	read -p "Coloque su nueva Netmask:" PNM
	read -p "Coloque su nuevo Gateway:" PGW
	read -p "Compruebe que los datos ingresados son correctos, Nueva IP:'$PIP', Netmask:'$PNM', Gateway:'$PGW'. Si son correctos presione <Enter> si no lo son para salir <CTRL+C>"
	sed s/dhcp/static/g /etc/network/interfaces
	echo "address $PIP" >> /etc/network/interfaces
	echo "netmask $PNM" >> /etc/network/interfaces
	echo "gateway $PGW" >> /etc/network/interfaces
	sleep 0.5s
	echo "Cambio de IP realizado"
	read -r -p "¿Quiere reinicar para aplicar los cambios? (S/N)" RESP
        case $RESP in
                [Ss] ) reboot;;
                [Nn] ) exit;;
                * ) echo "Por favor contestar con (S/N)";;
        esac
;;
modipstatic)
        echo "Si esta conectado remotamente probablemente pierda la conexion."
        read -p "Para poder hacer la modificacion de su ip estatica es necesario reiniciar para que se apliquen los cambios, continuar con el cambio <Enter> y para salir <CTRL+C>"
 	read -p "Coloque su IP actual:" APIP
        read -p "Coloque su Netmask actual:" APNM
        read -p "Coloque su Gateway actual:" APGW
	read -p "Compruebe que los datos ingresados son correctos ACTUAL: IP:'$APIP', Netmask:'$APNM', Gateway:'$APGW'. Si son correctos presione <Enter> si no lo son para salir <CTRL+C>"
	read -p "Coloque su nueva IP:" PIP
        read -p "Coloque su nueva Netmask:" PNM
        read -p "Coloque su nuevo Gateway:" PGW
        read -p "Compruebe que sus datos son correctos NUEVA: IP:'$PIP' Netmask:'$PNM' Gateway:'$PGW' Si son correctos presione <Enter> si no lo son para salir <CTRL+C>"
        sed s/$APIP/$PIP/g /etc/network/interfaces
	sed s/$APNM/$PNW/g /etc/network/interfaces
	sed s/$APGW/$PGW/g /etc/network/interfaces
	sleep 0.5s
	echo "Cambio de IP realizado"
        read -r -p "¿Quiere reinicar para aplicar los cambios? (S/N)" RESP
        case $RESP in
                [Ss] ) reboot;;
                [Nn] ) exit;;
                * ) echo "Por favor contestar con (S/N)";;
        esac
;;
modhostname)
	hostnamectl set-hostname $2
	read -r -p "¿Quiere reinicar para aplicar los cambios? (S/N)" RESP
	case $RESP in
        	[Ss] ) reboot;;
        	[Nn] ) exit;;
        	* ) echo "Por favor contestar con (S/N)";;
	esac
;;
insapache)
	echo "Instalando Apache"
        apt-get update > /dev/null 2>&1
        apt-get -y install apache2 > /dev/null 2>&1 && echo "Se instalo Apache."
;;
insapaphp)
	echo "Instalando Apache y PHP/MySQL"
        apt-get update > /dev/null 2>&1
        apt-get -y install apache2 > /dev/null 2>&1
	apt-get -y install default-mysql-server php libapache2-mod-php php-mysql
	systemctl restart apache2
	sleep 0.5s
	echo "Se instalo Apache y PHP/MySQL"
;;
desapache)
	read -p "¿Esta seguro de eliminar Apache? para confirmar presione <ENTER> para salir presione <CTRL+C>"
	apt-get -y remove --purge apache2
;;
desapaphp)
	read -p "¿Esta seguro de eliminar Apache y PHP/MySQL? para confirmar presione <ENTER> para salir presione <CTRL+C>"
	apt-get -y remove --purge apache2
	apt-get -y remove --purge php
	apt-get -y remove --purge php7
	apt-get -y remove --purge mysql
;;
iniciarapac)
        read -p "Esta seguro de iniciar Apache? Continuar <Enter> o para salir <CTRL+C>"
        systemctl start apache2
        sleep 0.5s
        echo "Apache deberia estar iniciado"
;;
reiniciarap)
	read -p "Esta seguro de reiniciar Apache? Continuar <Enter> o para salir <CTRL+C>"
	systemctl reload apache2
	sleep 0.5s
	echo "Apache deberia estar reiniciado"
;;
crearsitio)
	read -p "Esta seguro que quiere crear un sitio? Continuar <Enter> o para salir <CTRL+C>"
	read -p "Por favor coloque el sitio a crear (Ej: ejemplo.com):" SITIO
	echo "Creando los archivos necesarios"
	touch /etc/apache2/sites-available/$SITIO.conf
	echo "  <VirtualHost *:80>
	        ServerAdmin webmaster@localhost
		DocumentRoot /var/www/$SITIO
        	ServerName $SITIO
       		ErrorLog ${APACHE_LOG_DIR}/error.log
        	CustomLog /var/log/apache2/$SITIO.log combined
		</VirtualHost>" > /etc/apache2/sites-available/$SITIO.conf
	mkdir /var/www/$SITIO
	echo "$SPHP" > /var/www/$SITIO/info.php
	echo "Para probar la pagina, se generara un index.html"
	echo "<h1>Probando funcionamiento del script</h1>" > /var/www/$SITIO/index.html
	echo "Activando el sitio"
	a2ensite $SITIO > /dev/null 2>&1
	systemctl reload apache2 > /dev/null 2>&1
	sleep 0.5s
	read -p "Introduzca su ip para emular un dominio:" EDOM
	echo "$EDOM $SITIO" >> /etc/hosts
	echo "Su sitio $SITIO deberia estar activo sobre su IP correspondiente"
;;
altasitio)
	read -p "Ingrese el sitio a activar (Ej: ejemplo.com)" SITIO
	read -p "Esta seguro de activar el Sitio:'$SITIO'? Continuar <Enter> o para salir <CTRL+C>"
	a2ensite $SITIO.conf
	systemctl reload apache2
	sleep 0.5s
	echo "Se mando la activacion del sitio:'$SITIO'"
;;
bajasitio)
	read -p "Ingrese el sitio a desactivar (Ej: ejemplo.com)" SITIO
        read -p "Esta seguro de desactivar el Sitio:'$SITIO'? Continuar <Enter> o para salir <CTRL+C>"
	a2dissite $SITIO.conf
	systemctl reload apache2
	sleep 0.5s
	echo "Se mando la desactivacion del sitio:'$SITIO'"
;;
remsitiovh)
	read -p "Ingrese el sitio a Eliminar (Ej: ejemplo.com)" SITIO
        read -p "Esta seguro de Eliminar el Sitio:'$SITIO'? Continuar <Enter> o para salir <CTRL+C>"
        rm /etc/apache2/sites-avaible/$SITIO.conf
	systemctl reload apache2
	sleep 0.5s
        echo "El sitio:'$SITIO' deberia estar eliminado"
;;
remsitiodi)
        read -p "Ingrese el sitio a Eliminar (Ej: ejemplo.com)" SITIO
        read -p "Esta seguro de Eliminar el Sitio:'$SITIO'? Continuar <Enter> o para salir <CTRL+C>"
        rm -r /var/www/$SITIO/
        systemctl reload apache2
        sleep 0.5s
        echo "El sitio:'$SITIO' deberia estar eliminado"
;;
wordpress)
	read -p "Es importante que tenga instalado Apache y PHP/MySQL, si es así para continuar presione <ENTER> o para salir <CTRL+C>"
	echo "Iniciando la creacion de la base de datos 'wordpress'"
        mysql -e "CREATE DATABASE wordpress;"
        read -p "Coloque un nombre de usuario para su base de datos (Solo letras):" DBUSER
        read -p "Coloque la contraseña para el usuario de la base de datos (Primera letra Mayuscula y un Numero al final):" DBPASS
	echo "Tenga en cuenta esto: Nombre de usuario (Solo letras) y Contraseña (Primera letra Mayuscula y un Numero al final)"
	read -p "Confirme que sus datos son correctos: Usuario:'$DBUSER' Contraseña:'$DBPASS'. Si es así para continuar presione <ENTER> o para salir <CTRL+C>"
        mysql -e "GRANT ALL ON wordpress.* TO '$DBUSER'@'localhost' IDENTIFIED BY '$DBPASS';"
        echo "Usuario y base de datos creada"
        mysql -e "FLUSH PRIVILEGES;"
        read -p "Coloque el nombre del sitio (Ej: ejemplo.com): " SITIO
	read -p "Confirme que su sitio es el correcto: Sitio:'$SITIO'. Si es así para continuar presione <ENTER> o para salir <CTRL+C>"
        wget -P "/var/www/$SITIO" https://wordpress.org/latest.tar.gz
        tar -x -f /var/www/$SITIO/latest.tar.gz -C /var/www/$SITIO
	cp /var/www/$SITIO/wordpress/wp-config-sample.php /var/www/$SITIO/wordpress/wp-config.php
	systemctl reload apache2
	sleep 0.5s
	echo "Su sitio wordpress ya esta terminado"
;;
backupstvh)
	read -p "Ingrese el sitio a hacer Backup (Ej: ejemplo.com)" SITIO
        read -p "Ingrese la ruta de donde quiere que se guarde el backup" RUTA
	read -p "Esta seguro de hacer Backup del Sitio:'$SITIO' en la ubicacion:'$RUTA'? Continuar <Enter> o para salir <CTRL+C>"
	mkdir -p $RUTA
	tar cvzf /etc/apache2/sites-avaible/$SITIO.conf.$(date +%d-%m-%y_%H.%M.%S).tar.gz --absolute-names $RUTA
	echo "El backup a finalizado a las: $(date +%T)"
;;
backupstdi)
	read -p "Ingrese el sitio a hacer Backup (Ej: ejemplo.com)" SITIO
        read -p "Ingrese la ruta de donde quiere que se guarde el backup" RUTA
        read -p "Esta seguro de hacer Backup del Sitio:'$SITIO' en la ubicacion:'$RUTA'? Continuar <Enter> o para salir <CTRL+C>"
        mkdir -p $RUTA
        tar cvzf /var/www/$SITIO.$(date +%d-%m-%y_%H.%M.%S).tar.gz --absolute-names $RUTA
        echo "El backup a finalizado a las: $(date +%T)"
;;
migrarscpvh)
	echo "Importante que tenga las configuraciones correctas en el servidor destino para permitir la migracion"
	read -p "Ingrese el sitio a migrar (Ej: ejemplo.com)" SITIO
	read -p "Ingrese el usuario del servidor destino" DUSER
	read -p "Ingrese la IP del servidor destino" DIP
	read -p "Esta seguro de migrar el Sitio:'$SITIO' a el servidor con usuario:'$DUSER' y IP:'$DIP'? Continuar <Enter> o para salir <CTRL+C>"
	scp /etc/apache2/sites-avaible/$SITIO.conf $DUSER@$DIP:/etc/apache2/sites-avaible/
	sleep 1s
	echo "Migracion finalizada"
;;
migrarscpdi)
 	echo "Importante que tenga las configuraciones correctas en el servidor destino para permitir la migracion"
        read -p "Ingrese el sitio a migrar (Ej: ejemplo.com)" SITIO
        read -p "Ingrese el usuario del servidor destino" DUSER
        read -p "Ingrese la IP del servidor destino" DIP
        read -p "Esta seguro de migrar el Sitio:'$SITIO' a el servidor con usuario:'$DUSER' y IP:'$DIP'? Continuar <Enter> o para salir <CTRL+C>"
        scp -r /var/www/$SITIO/ $DUSER@$DIP:/var/www/
        sleep 1s
        echo "Migracion finalizada"
;;
altasitiore)
	read -p "Ingrese el sitio a activar (Ej: ejemplo.com)" SITIO
        read -p "Ingrese el usuario del servidor destino" DUSER
        read -p "Ingrese la IP del servidor destino" DIP
        read -p "Esta seguro de activar el Sitio:'$SITIO' en el servidor con usuario:'$DUSER' y IP:'$DIP'? Continuar <Enter> o para salir <CTRL+C>"
	ssh $DUSER@$DIP 'a2ensite $SITIO.conf'
	ssh $DUSER@$DIP 'systemctl reload apache2'
        sleep 1s
	echo "Se mando la activacion del sitio:'$SITIO' a el servidor remoto/destino:'$DIP'"
;;
esac
