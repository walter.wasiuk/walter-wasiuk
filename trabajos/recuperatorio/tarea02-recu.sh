#!/bin/bash

# Recuperatorio tarea 2
# Con este script se crearan backups de los usuarios del sistema.
# El backup se realizara en la carpeta /backupsusers con su respecitva fecha.

#Comprobacion de root

if [ $UID -ne 0 ]; then
	echo "Por favor, ejecute este script como usuario 'root'"
	exit 1
fi

# Variables
SDATE=`date +%d-%m-%y_%H.%M.%S`
ORIGEN=$HOME/backupusers/
BFIL=$HOME/backupusers/$USER.$(date +%d-%m-%y_%H.%M.%S).tar.gz
BLOG=$HOME/backupusers/backuplogs.txt
BERR=$HOME/backupusers/backuperrores.txt

echo "Esta iniciando el script de backup de usuarios del sistema en $BAR"
read -p "Para continuar presione <ENTER> en caso contrario <Ctrl+C>"


# Guardado de la hora de inicio
echo "El inicio del backup es a las $(date +%T)"
echo "Backup inicializado: $SDATE" >>$BLOG

# Creacion de los directorios y archivos correspondientes.
mkdir -p $ORIGEN && touch $BLOG && touch $BERR && echo "Completado el mkdir y touch: $DATE" >> $BLOG

#Backup y fecha de finalizacion
tar cvzf $BFIL --absolute-names $HOME 1>>$BLOG 2>>$BERR && echo "Completado el tar: $DATE" >> $BLOG
echo "El backup a finalizado a las: $(date +%T)"
echo "Backup finalizado: $(date)" >>$BLOG
echo "" >>$BLOG

#Siguiente punto
read -p "Para continuar con el guardado de Login de Usuarios presione <ENTER> en caso contrario <CTRL+C>"

clear

#Variables
TODOS="/home/todos"
TODOSL="/home/todos/login.log"
USUARIO="$HOME/usuario"
USUARIOL="$HOME/usuario/login.log"

# Creacion de los directorios, archivos y permisos correspondientes
mkdir -p $TODOS
chmod o+w $TODOS
mkdir -p $USUARIO
touch $TODOSL
chmod o+w $TODOSL
touch $USUARIOL

#Variable exportada Logindate en etc/profile
echo export LOGINDATE=\$\(date\) >> /etc/profile

#Carga de informacion
echo "$USER a ingresado al sistema el: $LOGINDATE" >> $TODOSL
echo "$USER a ingresado al sistema el: $LOGINDATE" >> $USUARIOL
echo "Se almaceno la informacion de inicio de sesion"

#Informacion
echo "Se realizo el guardado de Login de Usuarios en $TODOSL y $USUARIOL"
echo "Si no usaste el script antes, funcionara la proxima vez que lo hagas"

#Tercera parte y final

read -p "Para continuar con el script que suma días con horas presione <ENTER> en caso contrario <CTRL+C>"

SDIA=`date +%d`
SMINUTO=`date +%M`

export SDIA=`date +%d`
export SMINUTO=`date +%M`

read -p "Aprete enter para llamar al segundo script y hacer la suma"

bash scriptsuma.sh
