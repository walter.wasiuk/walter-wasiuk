#!/bin/bash


#TP Final
#Fecha: 17/12/2021
#Nombre y Apellido: Walter David Wasiuk
#Materia: Programacion
#Profesor: Sergio Pernas


#Comprobacion de root
if [ $UID -ne 0 ]; then
	echo "Por favor, ejecute este script como usuario 'root'"
	exit 1
fi



#Variables
ARGU=$1

#Comandos
echo "Los comandos que puede utilizar para manejar el script son los siguientes:"
echo "(Comando    = Que hace)"
echo "permisos    = Permite cambiar los permisos del sistema de usuarios/archivos"
echo "backup      = Permite la creacion de backups de ciertos ficheros"
echo "firewall    = Firewall del script anterior (conf. basica)"
echo "grupos      = Permite crear/eliminar grupos y buscar archivos por grupos"
echo ""
echo "EJEMPLO:'sudo ./final.sh permisos'"
echo ""

#Ejecucion
case $ARGU in
permisos)
	echo "Tiene diferentes tipos de permisos a elegir por favor coloque el que necesite"
	echo "1) Permisos de RWX (numeros) en archivos"
	echo "2) Cambiar Dueño/grupo/directorios"
	read -p "Eliga una opcion (numero): " OPCION
	while [[ $OPCION -eq 0 || $OPCION -ge 7 ]]
			do
				echo "Ingreso un numero valido, son entre 1 y 6"
				echo -n "Elija el numero: "
				read "Eliga una opcion (numero):" OPCION
			done
	case $OPCION in
	1)
		read -p "Esta ejecutando el script de Permisos para continuar presione <ENTER> y para salir <CTRL+C>"
		read -p "Escriba la ruta completa del archivo o directorio que quiere cambiarle los permisos: " PFILE
		echo "IMPORTANTE Permisos: La forma de funcionar es la siguiente: primer lugar user, segundo lugar grupo y tercer lugar otros usuarios"
		sleep 0.5s
		read -p "Ahora escriba el conjunto de permisos (en numeros): " PERM
		read -r -p "Estas seguro que quieres cambiar $PFILE a permisos $PERM? (S/N): " RES
		case $RES in
	        	[Ss] ) chmod $PERM $PFILE > /dev/null 2>&1;;
	        	[Nn] ) exit;;
	        	* ) echo "Por favor contestar con (S/N)";;
		esac
		sleep 0.5s
		echo "Asi quedo su archivo ''$(ls -l $PFILE)''"
		sleep 0.5s
		;;
	2)
		GRD="/etc/group"
		PASW="/etc/passwd"

		read -p "Ahora sigue el proceso de cambiar dueño/grupo/directorios para continuar presione <ENTER> y para salir <CTRL+C>"
		read -p "Coloque la ruta del archivo/directorio para cambiar el dueño y/o grupo: " FDFIND
		read -p "Coloque el dueño/usuario nuevo para el archivo (Deje en blanco si no quiere cambiar): " FUSER
		if [ ! -z $FUSER ]; then
			if grep $FUSER $PASW > /dev/null 2>&1; then
			echo "Usuario elegido: $FUSER"
			else
       		         	echo "Por favor, coloque un usuario existente"
             		   	read -p "Escriba el nombre del usuario: " FUSER
		        fi
		fi

		read -p "Coloque el grupo para el archivo (Deje en blanco si no quiere cambiar): " FGR

		if [ ! -z $FGR ]; then
		        if grep $FGR $GRD > /dev/null 2>&1; then
			echo "Grupo elegido: $FGR"
        		else
                	echo "Por favor, coloque un grupo existente"
                	read -p "Coloque el nombre del grupo: " FGR
        		fi
		fi

		read -r -p "Estas seguro que quieres cambiar el dueño a '$FUSER' grupo a '$FGR' en el archivo/directorio '$FDFIND?' (S/N): " RESPPP
		case $RESPPP in
	        	[Ss] ) sudo chown $FUSER:$FGR $FDFIND;;
        		[Nn] ) exit;;
	        	* ) echo "Por favor contestar con (S/N)";;
		esac
		;;
	*)
	echo "La opcion que selecciono es invalida"
	;;
	esac
;;
backup)
	echo "Esta iniciando la seccion de backup"
	read -p "Para continuar presione <ENTER> en caso contrario <Ctrl+C>"
	read -p "Coloque la direccion completa de la carpeta que quiere RESGUARDAR: " ORIGEN
	read -p "Coloque la direccion completa de la carpeta en donde quiere que se GUARDE el backup: " GDIREC
	BFIL=$GDIREC/backup.$USER.$(date +%d-%m-%y_%H.%M.%S).tar.gz
	echo "El inicio del backup es a las $(date +%T)"
	# Creacion de los directorios correspondientes.
	mkdir -p $GDIREC
	#Backup
	tar cvzf $BFIL --absolute-names $ORIGEN
	sleep 0.5s
	echo "El backup a finalizado a las: $(date +%T)"
;;
firewall)
	echo "Esta es una version mas basica del firewall anterior"
	read -p "Si quiere ver las opciones y si es necesario instalar ufw presione <Enter> y para salir del menu <CTRL+C>"
	#Comprobacion de que tenga instalado UFW en el sistema
	if ! which ufw > /dev/null 2>&1 ; then
        	echo "Instalando el UFW"
        	apt-get update > /dev/null 2>&1
        	apt-get install ufw > /dev/null 2>&1 && echo "Se instalo el UFW"
	fi
	sleep 0.5s
	cat ./scriptfw/puntosfw.txt
	sleep 2s
	read -p "Seleccione una opcion: " OPPP
	case $OPP in
	prender)
        ufw enable
	;;
	apagar)
        ufw disable
	;;
	reglas)
        ufw show added
	;;
	estado)
        ufw status verbose
	;;
	limpiar)
        ufw reset
	;;
	*)
	echo "El comando ingresado es incorrecto"
	;;
	esac
;;
grupos)
	echo "Estas son los comandos que puede utilizar"
	echo "(comando   = Que hace)"
	echo "addrem    = Crear/eliminar grupos "
	echo "listar    = Listar archivos segun los grupos del sistema"
	echo ""
	sleep 2s
	read -p "Seleccione una opcion: " SEL
	case $SEL in
	addrem)
		GRD="/etc/group"
		read -p "Para seguir con la creacion/eliminacion de grupos presione <ENTER> y para salir <CTRL+C>"
		read -p "Coloque el nombre de grupo a crear (Deje en blanco si no quiere crear): " ADDG
		if [ ! -z $ADDG ]; then
        		groupadd $ADDG > /dev/null 2>&1 && \
        		echo "Grupo creado: " && cat $GRD | grep $ADDG
		fi
		sleep 0.5s
		read -p "Coloque el nombre de grupo a eliminar (Deje en blanco si no quiere eliminar): " REMG
		if [ ! -z $REMG ]; then
        		if grep $REMG $GRD > /dev/null 2>&1 ; then
                		read -r -p "Estas seguro que quieres eliminar el grupo '$REMG?' (S/N): " RESP
        		else
                		echo "El grupo que a colocado no existe"
                		read -p "Coloque el nombre de grupo a eliminar: " REMG
               			read -r -p "Estas seguro que quieres eliminar el grupo '$REMG?' (S/N): " RESP
        		fi
		fi
		case $RESP in
        		[Ss] ) groupdel $REMG; echo "Grupo '$REMG' eliminado";;
        		[Nn] ) exit;;
        		* ) echo "Por favor contestar con (S/N)";;
		esac
		sleep 0.5s
	;;
	listar)
		read -p "Ahora se van a listar los grupos del sistema para continuar presione <ENTER> y para salir <CTRL+C>"
		echo "Grupos del sistema:"
		cat -n /etc/group
		sleep 1s
		read -p "Ahora se van a poder buscar archivos segun permisos para continuar presione <ENTER> y para salir <CTRL+C>"
		read -p "Coloque la ruta para buscar el archivo: " FFILE
		read -p "Coloque los permisos para la busqueda (en numeros): " FFPER
		read -r -p "Estas seguro que quieres buscar el archivo '$FFILE' con los permisos '$FFPER?' (S/N): " RESPPPP
		case $RESPPPP in
        		[Ss] ) find $FFILE -perm $FFPER;;
        		[Nn] ) exit;;
        		* ) echo "Por favor contestar con (S/N)";;
		esac
		sleep 0.5s
	;;
	*)
	read "Por favor seleccione una opcion valida"
	;;
	esac
;;
esac
