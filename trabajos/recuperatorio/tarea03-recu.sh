#!/bin/bash

# Tarea N3 Recuperatorio

#Comprobacion de root

if [ $UID -ne 0 ]; then
	echo "Por favor, ejecute este script como usuario 'root'"
	exit 1
fi


#Permisos rwx (con numeros)

read -p "Esta ejecutando el script de Permisos para continuar presione <ENTER> y para salir <CTRL+C>"
read -p "Escriba la ruta completa del archivo o directorio que quiere cambiarle los permisos: " PFILE

echo "IMPORTANTE Permisos: La forma de funcionar es la siguiente: primer lugar user, segundo lugar grupo y tercer lugar otros usuarios"

sleep 0.5s

read -p "Ahora escriba el conjunto de permisos (en numeros): " PERM
read -r -p "Estas seguro que quieres cambiar $PFILE a permisos $PERM? (S/N): " RES

case $RES in
        [Ss] ) chmod $PERM $PFILE > /dev/null 2>&1;;
        [Nn] ) exit;;
        * ) echo "Por favor contestar con (S/N)";;
esac

sleep 0.5s

echo "Asi quedo su archivo ''$(ls -l $PFILE)''"

sleep 0.5s


# Cambiar Dueño/grupo/directorios

GRD="/etc/group"
PASW="/etc/passwd"

read -p "Ahora sigue el proceso de cambiar dueño/grupo/directorios para continuar presione <ENTER> y para salir <CTRL+C>"
read -p "Coloque la ruta del archivo/directorio para cambiar el dueño y/o grupo: " FDFIND
read -p "Coloque el dueño/usuario nuevo para el archivo (Deje en blanco si no quiere cambiar): " FUSER

if [ ! -z $FUSER ]; then

        if grep $FUSER $PASW > /dev/null 2>&1; then
	echo "Usuario elegido: $FUSER"
        else
                echo "Por favor, coloque un usuario existente"
                read -p "Escriba el nombre del usuario: " FUSER
        fi
fi

read -p "Coloque el grupo para el archivo (Deje en blanco si no quiere cambiar): " FGR

if [ ! -z $FGR ]; then

        if grep $FGR $GRD > /dev/null 2>&1; then
	echo "Grupo elegido: $FGR"
        else
                echo "Por favor, coloque un grupo existente"
                read -p "Coloque el nombre del grupo: " FGR
        fi
fi

read -r -p "Estas seguro que quieres cambiar el dueño a '$FUSER' grupo a '$FGR' en el archivo/directorio '$FDFIND?' (S/N): " RESPPP

case $RESPPP in
        [Ss] ) sudo chown $FUSER:$FGR $FDFIND;;
        [Nn] ) exit;;
        * ) echo "Por favor contestar con (S/N)";;
esac

echo "Asi quedo su archivo ''$(ls -l $FDFIND)''"

sleep 0.5s


# Creacion/Eliminado de Grupos

# Variable
GRD="/etc/group"

read -p "Ahora sigue la creacion/eliminacion de grupos para continuar presione <ENTER> y para salir <CTRL+C>"
read -p "Coloque el nombre de grupo a crear (Deje en blanco si no quiere crear): " ADDG

if [ ! -z $ADDG ]; then
        groupadd $ADDG > /dev/null 2>&1 && \
        echo "Grupo creado: " && cat $GRD | grep $ADDG
fi

sleep 0.5s

read -p "Coloque el nombre de grupo a eliminar (Deje en blanco si no quiere eliminar): " REMG

if [ ! -z $REMG ]; then
        if grep $REMG $GRD > /dev/null 2>&1 ; then
                read -r -p "Estas seguro que quieres eliminar el grupo '$REMG?' (S/N): " RESP
        else
                echo "El grupo que a colocado no existe"
                read -p "Coloque el nombre de grupo a eliminar: " REMG
               	read -r -p "Estas seguro que quieres eliminar el grupo '$REMG?' (S/N): " RESP
        fi
fi

case $RESP in
        [Ss] ) groupdel $REMG; echo "Grupo '$REMG' eliminado";;
        [Nn] ) exit;;
        * ) echo "Por favor contestar con (S/N)";;
esac

sleep 0.5s


# Listado de grupos del sistema

read -p "Ahora se van a listar los grupos del sistema para continuar presione <ENTER> y para salir <CTRL+C>"

echo "Grupos del sistema:"
cat -n /etc/group

sleep 1s

read -p "Ahora se van a poder buscar archivos segun permisos para continuar presione <ENTER> y para salir <CTRL+C>"
read -p "Coloque la ruta para buscar el archivo: " FFILE
read -p "Coloque los permisos para la busqueda (en numeros): " FFPER

read -r -p "Estas seguro que quieres buscar el archivo '$FFILE' con los permisos '$FFPER?' (S/N): " RESPPPP

case $RESPPPP in
        [Ss] ) find $FFILE -perm $FFPER;;
        [Nn] ) exit;;
        * ) echo "Por favor contestar con (S/N)";;
esac

sleep 0.5s

echo "El exit status del programa es: $? "
