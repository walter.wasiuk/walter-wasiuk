#!/bin/bash

#Con este script se crearan backups de los usuarios del sistema.
#El backup se realizara en la carpeta /backupsusers con su respecitva fecha.

#Variables

DATE=`date +%d-%m-%y_%H.%M`
FDEBACKUP=backup$DATE
ARR=/logsbackupsusers/
INF=/logsbackupsusers/informe.txt
ERR=/logsbackupsusers/errores.txt

#Crear carpetas correspondientes
echo Para poder hacer el backup se necesita permisos de administrador, coloque la contraseña cada vez que se la requiera.
su - -c "mkdir -p $ARR"
su - -c "touch $INF $ERR"

#Backup
su - -c "echo La fecha y hora de inicio del backup es $DATE 2>> $ERR | tee -a $INF"
su - -c "cp -r /home /backupsusers/$FDEBACKUP 2>> $ERR | tee -a $INF"
su - -c "echo La fecha y hora de finalizacion del backup es $DATE 2>> $ERR | tee -a $INF"
