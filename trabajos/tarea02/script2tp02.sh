#!/bin/bash

#Variables
DATE=`date +%d-%m-%y_%H.%M`
TODOS="/home/todos"
TODOSL="/home/todos/userslogin.log"
USUARIO="$HOME/usuario"
USUARIOL="$HOME/usuario/login.log"

#Creacion de carpetas y logs
echo "Ingrese la constraseña de administrador las veces que sea requerida."
su - -c "mkdir -p $TODOS"
su - -c "mkdir -p $USUARIO"
su - -c "touch $TODOSL"
su - -c "touch $USUARIOL"

#Variable Logindate en etc/profile
su - -c "echo export LOGINDATE=$($DATE) >> /etc/profile"

#Variable que se exporta
export LOGINDATE=$($DATE)

#Carga de informacion
su - -c "echo "$USER a ingresado al sistema el: $LOGINDATE" >> $TODOSL"
su - -c "echo "$USER a ingresado al sistema el: $LOGINDATE" >> $USUARIOL"
echo "Se almaceno la informacion de inicio de sesion"
